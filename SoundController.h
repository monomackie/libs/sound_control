#pragma once

#include <unordered_map>
#include <endpointvolume.h>
#include <audiopolicy.h>
#include <vector>

#define SPEAKER_MODE 1
#define MICROPHONE_MODE 2
#define SYSTEM_SOUNDS "System sounds"
#define UNKNOWN_NAME "Unknown app"

class SoundController {
private:
    IAudioEndpointVolume                    *m_Endpoint;
    IAudioSessionEnumerator                 *m_SessionEnumerator;
    IAudioSessionManager2                   *m_SessionManager;
    std::unordered_map<std::string, int>    m_SessionMap;
    static std::string getNameFromId(std::string);

public:
    explicit SoundController(int mode);
    ~SoundController();
    void    updateSessionEnumerator();
    void    setSessionVolume(const std::string& sessionName, float volume);
    void    setSessionVolume(int sessionID, float volume);
    void    setMasterVolume(float volume);
    void    closeDevice();
    void    printSessionsInfo();
    float   getSessionVolume(const std::string& sessionName);
	float   getSessionVolume(int sessionID);
	float   getMasterVolume();
	int     getSessionCount();
	std::vector<std::string> getSessionsName();

    std::string getSessionName(std::vector<std::basic_string<char>> *sessionsNames, wchar_t *&wchar_tmp, char *char_tmp,
                               IAudioSessionControl *&m_pAudioSession, IAudioSessionControl2 *&m_pAudioSession2, int i);

    static std::string
    getSessionNameFromID(std::vector<std::basic_string<char>> *sessionsNames, wchar_t *&wchar_tmp, char *char_tmp,
                         IAudioSessionControl *&m_pAudioSession, IAudioSessionControl2 *&m_pAudioSession2,
                         std::string &displayName) ;
};

