#include "SoundController.h"
#include <mmdeviceapi.h>
#include <audioclient.h>
#include <audiopolicy.h>
#include <vector>

SoundController::SoundController(int mode) :
        m_Endpoint(nullptr),
        m_SessionEnumerator(nullptr),
        m_SessionManager(nullptr) {
    IMMDevice *pDevice = nullptr;
    IMMDeviceEnumerator *pDeviceEnumerator = nullptr;

    CoInitialize(nullptr);
    CoCreateInstance(
            __uuidof(MMDeviceEnumerator),
            nullptr,
            CLSCTX_INPROC_SERVER,
            __uuidof(IMMDeviceEnumerator),
            (LPVOID *) &pDeviceEnumerator
    );

    if (mode == SPEAKER_MODE) {
        pDeviceEnumerator->GetDefaultAudioEndpoint(
                eRender,
                eConsole,
                &pDevice
        );
    } else if (mode == MICROPHONE_MODE) {
        pDeviceEnumerator->GetDefaultAudioEndpoint(
                eCapture,
                eConsole,
                &pDevice
        );
    }

    pDevice->Activate(
            __uuidof(IAudioSessionManager2),
            CLSCTX_ALL,
            nullptr,
            (void **) &m_SessionManager
    );

    m_SessionManager->GetSessionEnumerator(&m_SessionEnumerator);
    wchar_t *wchar_tmp;
    char char_tmp[256];
    IAudioSessionControl *m_pAudioSession;
    for (int i = 0; i < getSessionCount(); i++) {
        m_SessionEnumerator->GetSession(i, &m_pAudioSession);
        m_pAudioSession->GetDisplayName(&wchar_tmp);
        sprintf(char_tmp, "%ws", wchar_tmp);
        m_SessionMap[std::string(char_tmp)] = i;
    }

    pDevice->Activate(
            __uuidof(IAudioEndpointVolume),
            CLSCTX_INPROC_SERVER,
            nullptr,
            (LPVOID *) &m_Endpoint
    );

    pDevice->Release();
    pDeviceEnumerator->Release();
}

SoundController::~SoundController() {
    closeDevice();
}

void SoundController::closeDevice() {
    m_SessionManager->Release();
    m_Endpoint->Release();
    CoUninitialize();
}

/**
 * Updates the state of m_SessionMap to keep up with the sessions changes
 */
void SoundController::updateSessionEnumerator() {
    std::unordered_map<std::string, int> new_map;
    std::vector<std::string> added;
    std::vector<std::string> removed;
    m_SessionManager->GetSessionEnumerator(&m_SessionEnumerator);
    wchar_t *wchar_tmp;
    char char_tmp[256];
    IAudioSessionControl *m_pAudioSession;
    for (int i = 0; i < getSessionCount(); i++) {
        m_SessionEnumerator->GetSession(i, &m_pAudioSession);
        m_pAudioSession->GetDisplayName(&wchar_tmp);
        sprintf(char_tmp, "%ws", wchar_tmp);
        std::string session_name(char_tmp);
        new_map[session_name] = i;

        if (m_SessionMap.find(session_name) == m_SessionMap.end()) {
            added.push_back(session_name);
        }
    }

    for (const auto &pair: m_SessionMap) {
        if (new_map.find(pair.first) == new_map.end()) {
            removed.push_back(pair.first);
        }
    }

    //TODO : do stuff with added & removed vectors
    m_SessionMap = new_map;
}

void SoundController::setSessionVolume(const std::string &sessionName, float volume) {
    IAudioSessionControl *m_pAudioSession;
    ISimpleAudioVolume *pSimpleAudioVolume;
    m_SessionEnumerator->GetSession(m_SessionMap[sessionName], &m_pAudioSession);
    m_pAudioSession->QueryInterface(IID_PPV_ARGS(&pSimpleAudioVolume));
    pSimpleAudioVolume->SetMasterVolume(volume, nullptr);
}

void SoundController::setSessionVolume(int sessionID, float volume) {
    IAudioSessionControl *m_pAudioSession;
    ISimpleAudioVolume *pSimpleAudioVolume;
    m_SessionEnumerator->GetSession(sessionID, &m_pAudioSession);
    m_pAudioSession->QueryInterface(IID_PPV_ARGS(&pSimpleAudioVolume));
    pSimpleAudioVolume->SetMasterVolume(volume, nullptr);
}

void SoundController::setMasterVolume(float volume) {
    m_Endpoint->SetMasterVolumeLevelScalar(volume, nullptr);
}

float SoundController::getMasterVolume() {
    float volume;
    m_Endpoint->GetMasterVolumeLevelScalar(&volume);
    return volume;
}

/**
 * Print each session's name and ID
 * @warning make sure to call updateSessionEnumerator() beforehand
 */
void SoundController::printSessionsInfo() {
    wchar_t *wchar_displayName;
    wchar_t *wchar_ID;
    char char_displayName[256];
    char char_ID[256];
    IAudioSessionControl *m_pAudioSession;
    IAudioSessionControl2 *m_pAudioSession2;
    for (int i = 0; i < getSessionCount(); i++) {
        printf("Session %02d:\n", i);
        m_SessionEnumerator->GetSession(i, &m_pAudioSession);
        m_pAudioSession->QueryInterface(IID_PPV_ARGS(&m_pAudioSession2));
        m_pAudioSession2->GetSessionIdentifier(&wchar_ID);
        m_pAudioSession->GetDisplayName(&wchar_displayName);

        sprintf(char_displayName, "%ws", wchar_displayName);
        printf("\tDisplayName   : %s\n", char_displayName);

        sprintf(char_ID, "%ws", wchar_ID);
        printf("\tID   : %s\n", char_ID);

        m_pAudioSession->GetIconPath(&wchar_displayName);
        sprintf(char_displayName, "%ws", wchar_displayName);
        printf("\tIconPath      : %s\n", char_displayName);

        printf("\tVolume        : %f\n", getSessionVolume(i));
    }
    CoTaskMemFree(wchar_displayName);
    CoTaskMemFree(wchar_ID);
}

/**
 * Method that gets the names of the different sound apps running on the device
 */
std::vector<std::string> SoundController::getSessionsName() {
    std::vector<std::string> sessionsNames;
    wchar_t *wchar_tmp;
    char char_tmp[256];

    IAudioSessionControl *m_pAudioSession;
    IAudioSessionControl2 *m_pAudioSession2;
	updateSessionEnumerator();
    sessionsNames.reserve(getSessionCount());
for (int i = 0; i < getSessionCount(); i++) {
        sessionsNames.push_back(getSessionName(&sessionsNames, wchar_tmp, char_tmp, m_pAudioSession, m_pAudioSession2, i));
    }
    CoTaskMemFree(wchar_tmp);
    return sessionsNames;
}

/**
 * Get the name from 1 Session and puts it at the end of sessionsNames
 * @param sessionsNames the vector of names
 * @param wchar_tmp contains the display name then the ID of the session
 * @param char_tmp contains the same as @wchar_tmp but converted in char*
 * @param m_pAudioSession Interface created
 * @param m_pAudioSession2 Interface Created
 * @param i Index - Number of the session
 */
std::string SoundController::getSessionName(std::vector<std::basic_string<char>> *sessionsNames,
											wchar_t *&wchar_tmp,
                                     		char *char_tmp,
											IAudioSessionControl *&m_pAudioSession,
                                     		IAudioSessionControl2 *&m_pAudioSession2,
											 int i) {
    m_SessionEnumerator->GetSession(i, &m_pAudioSession);
    m_pAudioSession->GetDisplayName(&wchar_tmp);
    sprintf(char_tmp, "%ws", wchar_tmp);
    std::string displayName = std::string(char_tmp);
    if (displayName.find("AudioSrv.Dll") != std::string::npos) {
		return SYSTEM_SOUNDS;
	} else {
        return (getSessionNameFromID(sessionsNames, wchar_tmp, char_tmp, m_pAudioSession, m_pAudioSession2, displayName));
    }
}

std::string SoundController::getSessionNameFromID(std::vector<std::basic_string<char>> *sessionsNames,
												  wchar_t *&wchar_tmp,
												  char *char_tmp,
												  IAudioSessionControl *&m_pAudioSession,
												  IAudioSessionControl2 *&m_pAudioSession2,
												  std::string &displayName) {
    m_pAudioSession->QueryInterface(IID_PPV_ARGS(&m_pAudioSession2));
    m_pAudioSession2->GetSessionIdentifier(&wchar_tmp);
    sprintf(char_tmp, "%ws", wchar_tmp);
    displayName = std::string(char_tmp);
    if (displayName.find(".exe") != std::string::npos) {
        return getNameFromId(displayName);
    } else {
		return UNKNOWN_NAME;
	}
}

/**
 * Extract the name of the Session from an ID given by IaudioSessionControl2:GetSessionIdentifier
 * @param displayName is the ID gotten containing a ".exe" in it
 * @return the string with only the name of the app ( the part before the ".exe")
 */
std::string SoundController::getNameFromId(std::string displayName) {
    std::string appName;
    int slashPosition = 0;
    int pointPosition = 0;
    for (int i = 0; i < displayName.size(); i++) {
        if (displayName[i] == '\\') {
            slashPosition = i;
            pointPosition = i;
        }
        if (displayName[i] == '.') {
            pointPosition = i;
        }
    }
    slashPosition++;
    appName = displayName.substr(slashPosition, pointPosition - slashPosition);
    return appName;
}

/**
 * Return the number of sessions.
 * @warning make sure to call updateSessionEnumerator() beforehand
 * @return number of sessions
 */
int SoundController::getSessionCount() {
    int count;
    m_SessionEnumerator->GetCount(&count);
    return count;
}

/**
 * Return the volume of the given session
 * @param sessionID ID of the session
 * @warning make sure to call updateSessionEnumerator() beforehand
 * @return volume between 0.0 and 1.0 if the session exists, -1.0 otherwise
 */
float SoundController::getSessionVolume(int sessionID) {
    float volume = -1;
    IAudioSessionControl *m_pAudioSession;
    ISimpleAudioVolume *pSimpleAudioVolume;
    m_SessionEnumerator->GetSession(sessionID, &m_pAudioSession);
    m_pAudioSession->QueryInterface(IID_PPV_ARGS(&pSimpleAudioVolume));
    pSimpleAudioVolume->GetMasterVolume(&volume);
    return volume;
}

/**
 * Return the volume of the given session
 * @param sessionName name of the session
 * @warning make sure to call updateSessionEnumerator() beforehand
 * @return volume between 0.0 and 1.0 if the session exists, -1.0 otherwise
 */
float SoundController::getSessionVolume(const std::string &sessionName) {
    float volume = -1;
    IAudioSessionControl *m_pAudioSession;
    ISimpleAudioVolume *pSimpleAudioVolume;
    m_SessionEnumerator->GetSession(m_SessionMap[sessionName], &m_pAudioSession);
    m_pAudioSession->QueryInterface(IID_PPV_ARGS(&pSimpleAudioVolume));
    pSimpleAudioVolume->GetMasterVolume(&volume);
    return volume;
}
